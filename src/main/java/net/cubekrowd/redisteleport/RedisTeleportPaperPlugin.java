/*

    redisteleport
    Copyright (C) 2017  CubeKrowd Network

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package net.cubekrowd.redisteleport;

import com.destroystokyo.paper.event.server.ServerTickEndEvent;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.time.Instant;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.messaging.PluginMessageListener;
import org.spigotmc.event.player.PlayerSpawnLocationEvent;

public class RedisTeleportPaperPlugin extends JavaPlugin implements PluginMessageListener, Listener {
    // Since 1.13, plugin message channel names must be lower case and must be
    // namespaced. In 1.12.2 the maximum length of a channel name is 20
    // characters. For compatibility reasons this needs to be taken into
    // account.
    private static final String MSG_CHANNEL = "redisteleport:main";
    public static final int MAX_TIMEOUT_MILLIS = 5 * 1000;
    public record TeleportData(UUID target, long commandTime) {}
    public Map<UUID, TeleportData> queue;

    @Override
    public void onEnable() {
        queue = new HashMap<>();
        getServer().getMessenger().registerOutgoingPluginChannel(this, MSG_CHANNEL);
        getServer().getMessenger().registerIncomingPluginChannel(this, MSG_CHANNEL, this);
        Bukkit.getPluginManager().registerEvents(this, this);
    }

    @Override
    public void onDisable() {
        queue = null;
    }

    public Location tryGetTargetLocation(UUID player) {
        var data = queue.get(player);
        if (data != null) {
            if ((Instant.now().toEpochMilli() - data.commandTime) <= MAX_TIMEOUT_MILLIS) {
                var target = Bukkit.getPlayer(data.target);
                if (target != null) {
                    return target.getLocation();
                }
            } else {
                getLogger().info("Teleport timed out for " + player);
                queue.remove(player);
            }
        }
        return null;
    }

    public void tryTeleportPlayer(UUID player) {
        var dest = tryGetTargetLocation(player);
        var online = Bukkit.getPlayer(player);
        if (online != null && dest != null) {
            getLogger().info("Teleporting " + player + " (" + online.getName() + ")");
            queue.remove(player);
            online.teleport(dest, PlayerTeleportEvent.TeleportCause.COMMAND);
        }
    }

    // @NOTE(traks) run late, so other plugins don't override us
    @EventHandler(priority = EventPriority.HIGH)
    public void onSpawn(PlayerSpawnLocationEvent e) {
        var dest = tryGetTargetLocation(e.getPlayer().getUniqueId());
        if (dest != null) {
            getLogger().info("Setting spawn location for " + e.getPlayer().getUniqueId() + " (" + e.getPlayer().getName() + ")");
            queue.remove(e.getPlayer().getUniqueId());
            e.setSpawnLocation(dest);
        }
    }

    @EventHandler
    public void onTickEnd(ServerTickEndEvent e) {
        if (!queue.isEmpty()) {
            var keys = new HashSet<>(queue.keySet());
            for (var player : keys) {
                tryTeleportPlayer(player);
            }
        }
    }

    @Override
    public void onPluginMessageReceived(String channel, Player player, byte[] message) {
        try {
            DataInputStream in = new DataInputStream(new ByteArrayInputStream(message));
            String subchannel = in.readUTF();
            if (subchannel.equals("Teleport")) {
                UUID subject = UUID.fromString(in.readUTF());
                UUID dest = UUID.fromString(in.readUTF());
                var commandTime = in.readLong();
                queue.put(subject, new TeleportData(dest, commandTime));
                getLogger().info("Got teleport for " + subject + " to " + dest);
                tryTeleportPlayer(subject);
            }
        } catch (IOException | IllegalArgumentException e) {
            getLogger().log(Level.WARNING, "Failed to handle plugin message", e);
        }
    }
}
