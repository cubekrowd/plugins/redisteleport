/*

    redisteleport
    Copyright (C) 2017  CubeKrowd Network

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package net.cubekrowd.redisteleport;

import java.util.function.BiConsumer;
import java.util.stream.Collectors;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.TabExecutor;

public class TeleportCommand extends Command implements TabExecutor {

    private final RedisTeleportBungeeCordPlugin plugin;
    private final BiConsumer<CommandSender,String> offline;

    public TeleportCommand(RedisTeleportBungeeCordPlugin plugin) {
        super("btp", "bungeeteleport.tp", "bteleport", "bungeetp", "redistp");
        this.plugin = plugin;
        offline = (sender, s) -> sender.sendMessage(new ComponentBuilder("Error: The " + s + " player is offline!").color(ChatColor.RED).create());
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        ProxiedPlayer subject;
        ProxiedPlayer target;
        if (args.length == 1) {
            if (sender instanceof ProxiedPlayer) {
                subject = (ProxiedPlayer) sender;
            } else {
                sender.sendMessage(new ComponentBuilder("Error: This is a player-only command!").color(ChatColor.RED).create());
                return;
            }

            ProxiedPlayer pt = plugin.getProxy().getPlayer(args[0]);
            if (pt == null) {
                offline.accept(sender, "target");
                return;
            }
            target = pt;
        } else if (args.length == 2 && sender.hasPermission("bungeeteleport.tp.others")) {
            ProxiedPlayer pt = plugin.getProxy().getPlayer(args[0]);
            if (pt == null) {
                offline.accept(sender, "source");
                return;
            }
            subject = pt;

            pt = plugin.getProxy().getPlayer(args[1]);
            if (pt == null) {
                offline.accept(sender, "target");
                return;
            }
            target = pt;
        } else {
            sender.sendMessage(plugin.prefix + ChatColor.GREEN + "Running version v" + plugin.getDescription().getVersion());
            sender.sendMessage(plugin.prefix + ChatColor.DARK_PURPLE + "/btp " + ChatColor.LIGHT_PURPLE + "<target>");
            if (sender.hasPermission("bungeeteleport.tp.others")) {
                sender.sendMessage(plugin.prefix + ChatColor.DARK_PURPLE + "/btp " + ChatColor.LIGHT_PURPLE + "<source> <target>");
            }
            return;
        }

        plugin.teleportPlayer(subject, target);
    }

    @Override
    public Iterable<String> onTabComplete(CommandSender sender, String[] args) {
        return plugin.getProxy().getPlayers().stream()
                .map(pp -> pp.getName())
                .filter(u -> u.toLowerCase().startsWith(args[args.length - 1].toLowerCase()))
                .collect(Collectors.toSet());
    }

}
