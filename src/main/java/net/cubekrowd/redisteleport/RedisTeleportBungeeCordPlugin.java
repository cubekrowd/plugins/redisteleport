/*

    redisteleport
    Copyright (C) 2017  CubeKrowd Network

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package net.cubekrowd.redisteleport;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import java.time.Instant;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Plugin;

public class RedisTeleportBungeeCordPlugin extends Plugin {
    private static final String MSG_CHANNEL = "redisteleport:main";
    public final String prefix = ChatColor.DARK_GRAY + "[" + ChatColor.YELLOW + "Redis" + ChatColor.GOLD + "Teleport" + ChatColor.DARK_GRAY + "] ";

    @Override
    public void onEnable() {
        getProxy().registerChannel(MSG_CHANNEL);
        getProxy().getPluginManager().registerCommand(this, new TeleportCommand(this));
    }

    public void teleportPlayer(ProxiedPlayer subject, ProxiedPlayer dest) {
        var si = dest.getServer().getInfo();

        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF("Teleport");
        out.writeUTF(subject.getUniqueId().toString());
        out.writeUTF(dest.getUniqueId().toString());
        out.writeLong(Instant.now().toEpochMilli());
        dest.getServer().sendData(MSG_CHANNEL, out.toByteArray());

        // Move them to correct server. Note: connecting a player to a server
        // they are already on sends them the message "You are already connected
        // to this server!" on 1.14 with zero configuration. Therefore we check
        // before connecting.
        if (!si.getPlayers().contains(subject)) {
            subject.connect(si);
        }
    }
}
